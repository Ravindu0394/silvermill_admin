import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-post-category',
  templateUrl: './post-category.component.html',
  styleUrls: ['./post-category.component.css']
})


export interface PostCategoryComponent {
    post_cat_id?: string;
    post_cat_desc: string;
    post_cat_status: boolean;
}
export class PostCategoryComponent {
  todoCollectionRef: AngularFirestoreCollection<PostCategoryComponent>;
  todo$: Observable<PostCategoryComponent[]>;

  constructor(private afs: AngularFirestore) {
    this.todoCollectionRef = this.afs.collection<PostCategoryComponent>('todos');
    this.todo$ = this.todoCollectionRef.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as PostCategoryComponent;
        const id = action.payload.doc.id;
        return {id, data};
      });
    });
  }
}