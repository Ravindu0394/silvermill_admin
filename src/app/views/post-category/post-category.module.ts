import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostCategoryComponent} from './post-category.component';
import { RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', component: PostCategoryComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PostCategoryModule { }
