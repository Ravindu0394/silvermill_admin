import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable} from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export interface PostsComponent {
    post_id?: string;
    post_title: string;
    post_author: string;
    author_designation: string;
    post_desc: string;
    post_content: string;
    post_status: boolean;
    post_banner: string;
}
export class PostsComponent {
  todoPost: AngularFirestoreCollection<PostsComponent>;
  todo$: Observable<PostsComponent[]>;

  constructor(private afs: AngularFirestore) {
    this.todoPost = this.afs.collection<PostsComponent>('todos');
    this.todo$ = this.todoPost.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as PostsComponent;
        const id  = action.payload.doc.id;
        return{id, data};
      });
    });
  }
}

