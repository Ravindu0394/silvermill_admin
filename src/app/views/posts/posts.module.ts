import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsComponent} from './posts.component';
import { RouterModule, Routes} from '@angular/router';
import { AllPostsComponent } from './all-posts/all-posts.component';

const routes: Routes = [
    {path: '', component: PostsComponent}
];

@NgModule({
  declarations: [AllPostsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PostsModule { }
