import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {path: '', loadChildren: () => import('../login/login.module') .then( m => m.LoginModule)},
    {path: 'dashboard', loadChildren: () => import('../views/dashboard/dashboard.module') .then( m => m.DashboardModule)},
    {path: 'post', loadChildren: () => import('../views/posts/posts.module') .then( m => m.PostsModule)},
    {path: 'post-category', loadChildren: () => import('../views/post-category/post-category.module') .then( m => m.PostCategoryModule)},
    {path: 'logout', loadChildren: () => import('../logout/logout.module') .then( m => m.LogoutModule)},
    {path: 'change-password', loadChildren: () => import('../change-password/change-password.module') .then( m => m.ChangePasswordModule)},
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
