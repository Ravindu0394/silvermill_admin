import { Component, OnInit } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})


export class LoginComponent implements OnInit {

  constructor(private  authService: AngularFireAuth) { }
  ngOnInit() {
  }
}
