import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase} from 'angularfire2/database';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { PostsComponent } from './views/posts/posts.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import { PostCategoryComponent } from './views/post-category/post-category.component';
import { LogoutComponent } from './logout/logout.component';
import { ChangePasswordComponent } from './change-password/change-password.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PostsComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    PostCategoryComponent,
    LogoutComponent,
    ChangePasswordComponent,
    AngularFireDatabaseModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireDatabase,
    AngularFireModule.initializeApp(environment.firebase, 'silvermill-admin'),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
