import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireObject} from '@angular/fire/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'silvermill-admin';
  email: string;
  password: string;

  public login: AngularFireObject<login[]>;
  constructor(db: AngularFireDatabase) {
    this.login = db.login();  }
}
